# 1 Infrastrucure Layer Bootstrap

## Generate Initial SSH keys
    
    cd ssh
    ssh-keygen -t rsa -b 4096 -C "manage@cloud.playground"  -f ./id_rsa -q -N ""

## Bootstrap the VMs with Terraform

    cd ../1-infrastructure-layer
    terraform  -chdir=terraform apply
    terraform  -chdir=terraform refresh  
    terraform  -chdir=terraform output -json node_list | jq -r '.[] | (.name) + " ansible_host=" + (.network_interface[0].addresses[0])' > ../hosts

Make sure that the ../hosts file is properly populated with the IP addresses

If not, repeat the last 2 commands again
If yes, continue with the VM basic setup

    ansible-playbook 1-setup-playbook.yml

# 2 Platform Layer Bootstrap

## PKI initialization

    cd 2-platform-layer
    ansible-playbook 0.0-setup-pki-playbook.yml
    ansible-playbook 0.1-setup-server-certs-playbook.yml
    ansible-playbook 0.2-trust-ca-cert-playbook.yml
    ansible-playbook 0.3-install-server-certs-playbook.yml

Trust the ca.crt locally on your machine to avoid browser warnings
    
    ./certs/easy-rsa-master/easyrsa3/pki/ca.crt

## Initialize and start the Consul Nomad and Vault

    ansible-playbook 1.0-setup-base-platform-playbook.yml
    ansible-playbook 1.1-setup-consul-playbook.yml
    ansible-playbook 1.2-setup-nomad-playbook.yml
    ansible-playbook 1.3-setup-vault-playbook.yml

## Local environment DNS setup

Set your local machine DNS resolver to point to one of the Consul nodes as /etc/resolv.conf:
    
    nameserver <node01_IP>

OR Add the following to the /etc/hosts file

    <node01_IP>	traefik.service.consul vault.service.consul consul.service.consul nomad.service.consul prometheus.service.consul grafana.service.consul


After this, test the following URLs:
* https://consul.service.consul:8501/ui/
* https://nomad.service.consul:4646/ui/jobs
* https://vault.service.consul:8200

## Vault setup

Generate and Sign the Vault Intermedia cert request with easyrsa:
    
    ansible-playbook 1.4-setup-vault-gen-int-ca-csr-playbook.yml
    cd certs/easy-rsa-master/easyrsa3/
    ./easyrsa import-req ./vault_pki_intermediate.csr vault_pki_intermediate
    ./easyrsa sign-req ca vault_pki_intermediate
    cd ../../../

Import the Signed Intermediate Cert back to vault
    
    ansible-playbook 1.5-setup-vault-import-int-ca-playbook.yml

# 3 Application Layer

    ansible-playbook 1.0-generate-service-ingress-certificates-playbook.yml 
    ansible-playbook 1.1-install-service-ingress-certificates-playbook.yml

## Deploying the applications

    export NOMAD_ADDR=https://nomad.service.consul:4646
    levant deploy -var-file=./deployment/defaults.yml ./deployment/traefik-ingress/deploy.hcl
    levant deploy -var-file=./deployment/defaults.yml ./deployment/monitoring/deploy.hcl
    