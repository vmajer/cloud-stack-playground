# Stacks Overview
- VMs via qemu KVM
- Cloud orchestration / Hashicorp stack
    - service discovery
- metrics / monitoring

# Pre requisites

Install the following on the local machine

- terraform
- vault
- nomad
- consul
- levant
- ansible
- libvirtd
- kvm / qemu
- pip install jmespath

# Infrastructure Layer

Provisioner: Terraform / Ansible

libvirtd / QEMU / KVM

# Platform Layer

- Hashicorp Consul
  - Cloud internal DNS and service registry
- Hashicorp Nomad
- easyRSA as master / external CA
- Hashicorp Vault (PKI, intermediate CA / Secret store )
    - Server Certs for Ingress (Traefik)

# Application Deployment Layer

## AUX Applications

- Grafana
- Prometheus

## TODO: Business Application Examples
- DB: Postgres / CockroachDB
- Message Bus: rabbitmq
- Kotlin based Apps
- Python based Apps
- Golang based Apps
- Application metrics and tracing

# Resources
Vault PKI Intermediate CA:

    https://learn.hashicorp.com/
