resource "libvirt_network" "cloud_network" {
  name      = "cloudnet"
  mode      = "nat"
  autostart = true
  domain    = "cloud.local"
  addresses = ["10.10.10.0/24"]
  dns {
    enabled = true
  }
}

resource "libvirt_pool" "cloud-pool" {
  name = "cloud-pool"
  type = "dir"
  path = "/opt/cloud_pool/storage"
}

resource "libvirt_volume" "vm_base_image" {
  name   = "Debian-Base-Image"
  pool   = libvirt_pool.cloud-pool.name
  source = "https://cloud.debian.org/images/cloud/sid/daily/latest/debian-sid-generic-amd64-daily.qcow2"
}


data "template_file" "user_data" {
  template = file("${path.module}/templates/cloud_init.yml")
  vars     = {
    ssh_pub_key = "${file("../../ssh/id_rsa.pub")}"
  }
}

resource "libvirt_cloudinit_disk" "commoninit" {
  name      = "commoninit.iso"
  pool      = libvirt_pool.cloud-pool.name
  user_data = data.template_file.user_data.rendered
}

variable "node_list" {
  type    = list(string)
  default = ["node01", "node02", "node03"]
}

resource "libvirt_volume" "node_volume" {
  for_each       = toset(var.node_list)
  name           = each.key
  pool           = libvirt_pool.cloud-pool.name
  size           = 8589935616
  base_volume_id = libvirt_volume.vm_base_image.id
}

resource "libvirt_domain" "cloud_node" {
  for_each  = toset(var.node_list)
  name      = each.key
  memory    = "4096"
  vcpu      = 2
  cloudinit = libvirt_cloudinit_disk.commoninit.id

  network_interface {
    network_name = libvirt_network.cloud_network.name
  }

  disk {
    volume_id = libvirt_volume.node_volume[each.key].id
  }

  console {
    type        = "pty"
    target_type = "serial"
    target_port = "0"
  }

  graphics {
    type        = "spice"
    listen_type = "address"
    autoport    = true
  }
}
